package lesson7.codaline.dima.dimacodalinelesson7;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<User> {
    private final String TAG = "MainActivity";
    private User user;
    private Button button;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button)findViewById(R.id.key);
        textView = (TextView) findViewById(R.id.TextView);

        if (savedInstanceState != null){
            user = savedInstanceState.getParcelable("key");
            if (user != null){
                Log.e(TAG,"recovery AsynTask");
            }
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putString("key", "Sample");
                args.putInt("int", 199717);
                getSupportLoaderManager().initLoader(199717, args, MainActivity.this);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getSupportLoaderManager().getLoader(199717) != null){
            getSupportLoaderManager().initLoader(199717, null, this);
            Log.e(TAG, "onResume AsynTask");
        }
    }

    @Override
    public Loader<User> onCreateLoader(int id, Bundle args) {
        Log.e(TAG,"onCreateLoader");
        return new LoadMenager(this, args);
    }

    @Override
    public void onLoadFinished(Loader<User> loader, User data) {
        user = data;
        Log.e(TAG,"onLoadFinished AsynTask");
        //toDo();
        textView.setText(user.getName());
        getSupportLoaderManager().destroyLoader(7412);
    }

    @Override
    public void onLoaderReset(Loader<User> loader) {
        Log.e(TAG,"onLoaderReset AsynTask");
    }


}
