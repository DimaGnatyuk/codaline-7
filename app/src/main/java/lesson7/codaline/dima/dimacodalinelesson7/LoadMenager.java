package lesson7.codaline.dima.dimacodalinelesson7;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;

import java.util.concurrent.TimeUnit;

/**
 * Created by Admin on 26.03.2016.
 */
public class LoadMenager extends AsyncTaskLoader <User> {

    private User user;

    public LoadMenager(Context context,Bundle bundle) {
        super(context);
    }

    @Override
    public User loadInBackground() {
        user = new User();
        imitateHardWork();
        return user;
    }


    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if (user != null) {
            deliverResult(user);
        } else {
            forceLoad();
        }
    }

    public void imitateHardWork() {
        try {
            TimeUnit.SECONDS.sleep(10);
            user.setName("dima");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
